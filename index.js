console.log("JS-DOM Manipulation")

// [SECTION] : DOCUMENT OBJECT MODEL(DOM)

	//-allows us to access or modify the properties of an html element in a webpage.
	// It is standard oh how to get, change, add or delete HTML elements.
	// we will be focusing only with DOM in terms of managing forms

	// For selecting HTML elements we will be using the document.querySelector / getElementById 
		// Syntax:
			// document.querySelectorAll("html element")

		// CSS selector
			// class selector(.)
			// id selector (#)
			// tag selector (html tags)
			// universal selector(*)
			// attribute selector([attribute])


	let universalSelector = document.querySelectorAll("*");
	console.log(universalSelector);

	let singleUniversalSelector = document.querySelector("*");
	console.log(singleUniversalSelector);

	let classSelector = document.querySelectorAll(".full-name");
	console.log(classSelector);

	let singleClassSelector = document.querySelector(".full-name");
	console.log(singleClassSelector)

	let tagSelector = document.querySelectorAll("input");
	console.log(tagSelector);
	
	let spanSelector = document.querySelector("span[id]");
	console.log(spanSelector);


	// getElement
		let element = document.getElementById("fullName");
		console.log(element)

		element = document.getElementsByClassName("full-name");
		console.log(element);

// [SECTION] EVENT LISTENERS 
	// Whenever a user interacts with a webpage, this action is considered as an event
	// Working wiith events is large part of creating interactivity in a webpage
		// specific functions that will be triggred if the evenet happen.

		// The functions use is "addEventListener", it takes two argument
			// first argument a string identifying the event
			// second argument, function that the listener will be trigger once the "specified event" occur

		// 
		let txtFirstName = document.querySelector("#txt-first-name");

		// Add evenet listener
		// Add event listener

		txtFirstName.addEventListener("keyup", ()=>{
			console.log(txtFirstName.value);
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		})


		let txtLastName = document.querySelector("#txt-last-name");
		txtLastName.addEventListener("keyup", ()=>{
			spanSelector.innerHTML= `${txtFirstName.value} ${txtLastName.value}`
		})


		let changeColor = document.querySelector("#text-color");

		changeColor.addEventListener("click", ()=>{
			
			spanSelector.style.color = changeColor.value ;
			 
		})


		